package lianxi;

import java.util.Arrays;
import java.util.Comparator;

public class Code04_BestArrange {
    // 定义Program结构，start为开始时间，end为结束时间
    public static class Program {
        public int start;
        public int end;

        public Program(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    // 定义比较器，使项目按结束时间从小到大排序
    public static class ProgramComparator implements Comparator<Program> {
        @Override
        public int compare(Program o1, Program o2) {
            return o1.end - o2.end;
        }
    }

    // 主方法
    public static int bestArrange(Program[] programs, int start) {
        // 将项目按结束时间从小到大排序
        Arrays.sort(programs, new ProgramComparator());
        int result = 0;
        // 遍历项目，如果当前时间点小于项目的开始时间，则选择当前项目
        // 并将当前时间置为选择项目的结束时间，继续遍历
        for (int i = 0; i < programs.length; i++) {
            if (start <= programs[i].start) {
                result++;
                start = programs[i].end;
            }
        }
        return result;
    }

    // 测试函数
    public static void main(String[] args) {
        Program[] programs = new Program[6];
        programs[0] = new Program(0, 3);
        programs[1] = new Program(2, 4);
        programs[2] = new Program(4, 9);
        programs[3] = new Program(5, 7);
        programs[4] = new Program(6, 10);
        programs[5] = new Program(10, 12);
        int count = bestArrange(programs, 0);
        System.out.println("最多能安排" + count + "个项目");
    }
}
