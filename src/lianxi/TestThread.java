package lianxi;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TestThread {
    private int num = 10;
    private Lock lock = new ReentrantLock();
    private Object obj = new Object();

    public static void main(String[] args) {
        TestThread testThread = new TestThread();
        int n = 10;
        new Thread("xiancheng1") {
            @Override
            public void run() {
                for (int i = 0; i < n; i++) {
                    testThread.delete(Thread.currentThread().getName());
                }
            }
        }.start();

        new Thread("xiancheng2") {
            @Override
            public void run() {
                for (int i = 0; i < n; i++) {
                    testThread.delete(Thread.currentThread().getName());
                }
            }
        }.start();

    }

    public void delete(String name) {
        if (num > 0) {
            num--;
            System.out.println("线程" + name + "抢到票了！剩余票数：" + num);
        } else {
            System.out.println("线程" + name + "抢票失败！没有票了！");
        }
//        lock.lock();
//        try {
//            num--;
//            System.out.println("剩余票数：" + num);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            lock.unlock();
//        }
//        synchronized (obj) {
//            if (num > 0) {
//                num--;
//                System.out.println("线程" + name + "抢到票了！剩余票数：" + num);
//            } else {
//                System.out.println("线程" + name + "抢票失败！没有票了！");
//            }
//        }
    }
}
