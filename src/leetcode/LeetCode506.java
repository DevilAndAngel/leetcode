package leetcode;

public class LeetCode506 {
    public static String[] findRelativeRanks(int[] score) {
        String[] strs = new String[score.length];
        int index = 0;
        for (int i = 0; i < score.length; i++) {
            for (int j = 0; j < score.length; j++) {
                if (i != j && score[i] < score[j]) {
                    index++;
                }
            }
            switch (index) {
                case 0:
                    strs[i] = "Gold Medal";
                    break;
                case 1:
                    strs[i] = "Silver Medal";
                    break;
                case 2:
                    strs[i] = "Bronze Medal";
                    break;
                default:
                    strs[i] = String.valueOf(index + 1);
                    break;
            }
            index = 0;
        }
        return strs;
    }

    public static void main(String[] args) {
        findRelativeRanks(new int[]{10, 3, 8, 9, 4});
    }
}
