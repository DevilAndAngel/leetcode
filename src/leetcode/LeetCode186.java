package leetcode;

import java.util.*;

public class LeetCode186 {
    public int[] kthSmallestPrimeFraction(int[] arr, int k) {
        List<int[]> list = new ArrayList();
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j < arr.length; j++) {
                list.add(new int[]{arr[i], arr[j]});
            }
        }
        Collections.sort(list, (x, y) -> x[0] * y[1] - y[0] * x[1]);
        return  list.get(k - 1);
    }
}
