package leetcode;

public class LeetCode1816 {
    public static String truncateSentence(String s, int k) {
        String[] nums = s.split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < k; i++) {
            stringBuffer.append(nums[i]);
            if (i != k - 1) {
                stringBuffer.append(" ");
            }
        }
        return stringBuffer.toString();
    }


    public static void main(String[] args) {
        truncateSentence1("Hello how are you Contestant", 4);
    }
}
