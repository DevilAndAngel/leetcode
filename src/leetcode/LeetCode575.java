package leetcode;

import java.util.HashSet;
import java.util.Set;

public class LeetCode575 {
    public int distributeCandies(int[] candyType) {
        int snum = candyType.length / 2;
        Set<Integer> set = new HashSet();
        for (int i : candyType) {
            set.add(i);
        }
        return set.size() > snum ? snum : set.size();
    }
}
