package leetcode;

public class LeetCode42 {
    public int trap(int[] height) {
        if (height.length < 3) {
            return 0;
        }
        int count = 0;
        for (int i = 1; i < height.length - 1; i++) {
            int leftMax = 0;
            for (int left = 0; left < i; left++) {
                if (leftMax < height[left]) {
                    leftMax = height[left];
                }
            }
            int rightMax = 0;
            for (int right = i; right < height.length; right++) {
                if (rightMax < height[right]) {
                    rightMax = height[right];
                }
            }
            if (rightMax > leftMax) {
                count = count + (leftMax - height[i] < 0 ? 0 : leftMax - height[i]);
            } else {
                count = count + (rightMax - height[i] < 0 ? 0 : rightMax - height[i]);
            }
        }
        return count;
    }
}
