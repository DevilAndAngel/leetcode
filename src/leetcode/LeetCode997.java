package leetcode;

import java.util.Arrays;

public class LeetCode997 {
    public int[] sortedSquares(int[] nums) {
        nums = Arrays.stream(nums).map(num -> (int) Math.pow(num ,2)).sorted().toArray();
        return nums;
    }
}
