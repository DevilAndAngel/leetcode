package leetcode;

public class LeetCode397 {
    public int integerReplacement(int n) {
        int snum = 0;
        while (n != 1) {
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = (n + 1) / 2 % 2 == 0 ? n + 1 : n - 1;
            }
            snum++;
        }
        return snum;
    }
}
