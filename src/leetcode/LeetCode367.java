package leetcode;

public class LeetCode367 {
    public boolean isPerfectSquare(int num) {
        return Math.pow((int) Math.sqrt(num), 2) == num ? true : false;
    }
}
