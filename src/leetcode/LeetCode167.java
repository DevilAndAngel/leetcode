package leetcode;

import java.util.*;

public class LeetCode167 {
    public int[] twoSum(int[] numbers, int target) {
        Map<Integer, Integer> map = new HashMap();
        for (int i = 0; i < numbers.length; i++) {
            map.put(numbers[i], numbers[i]);
            if (map.containsKey(target - numbers[i])) {
                return new int[]{map.get(target - numbers[i]), i};
            }
        }
        return null;
    }
}
