package leetcode;

import java.util.concurrent.atomic.AtomicInteger;

public class LeetCode1117 {
    private AtomicInteger isok = new AtomicInteger(1);


    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
        while(isok.get() == 3){
            Thread.yield();
        }
        // releaseHydrogen.run() outputs "H". Do not change or remove this line.
        releaseHydrogen.run();
        isok.addAndGet(1);
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
        while(isok.get() != 3){
            Thread.yield();
        }
        // releaseOxygen.run() outputs "O". Do not change or remove this line.
        releaseOxygen.run();
        isok.set(1);
    }
}
