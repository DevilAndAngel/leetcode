package leetcode;

import java.util.Arrays;

public class LeetCode189 {
    public void rotate(int[] nums, int k) {
        if (k > nums.length) {
            k = nums.length % k;
        }
        int index = 0;
        int[] testNums = Arrays.copyOf(nums, nums.length);
        for (int i = 0; i < testNums.length; i++) {
            if (i < testNums.length - k) {
                nums[i + k] = testNums[i];
            } else {
                nums[0 + index] = testNums[i];
                index++;
            }
        }
    }
}
