package leetcode;

public class LeetCode1137 {
    public int tribonacci(int n) {
        if (n < 3) {
            return n;
        }

        int q=0,w=0,e=1,r=1;
        for (int i = 3;i<=n;i++) {
            q = w;
            w = e;
            e = r;
            r = q + w + e;
        }
        return r;
    }
}
