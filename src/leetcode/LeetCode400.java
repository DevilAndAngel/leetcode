package leetcode;

public class LeetCode400 {
    public int findNthDigit(int n) {
        int index = 0;
        int count = 0;
        while (n > count) {
            int testCount = count + 9 * (int) Math.pow(10, index) * (index + 1);
            if (n < testCount) {
                int a = (n - count) / (index + 1) + 9 * (int) Math.pow(10, index - 1);
                int b = (a - count) * (index + 1) + count - n;
                return b;
            }
            count = testCount;
            index++;
        }
        return n;
    }
}
