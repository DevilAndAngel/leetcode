package leetcode;

public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }

    public static ListNode buildListNode(int[] nums) {
        ListNode listNode = null;
        ListNode testNode;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (listNode == null) {
                listNode = new ListNode(nums[i]);
            } else {
                testNode = new ListNode(nums[i]);
                testNode.next = listNode;
                listNode = testNode;
            }
        }
        return listNode;
    }
}
