package leetcode;

public class LeetCode563 {
    int count = 0;

    public int findTilt(TreeNode root) {
        countNode(root);
        return count;
    }

    public int countNode(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int letf = countNode(root.left);
        int right = countNode(root.right);
        count += Math.abs(letf - right);
        return letf + right + root.val;
    }
}