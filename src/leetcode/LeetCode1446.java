package leetcode;

public class LeetCode1446 {
    public int maxPower(String s) {
        int max = 1, testMax = 1;
        char test = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            if (test == s.charAt(i)) {
                testMax++;
            } else {
                test = s.charAt(i);
                testMax = 1;
            }
            max = Math.max(max, testMax);
        }
        return max;
    }
}
