package leetcode;


import java.util.Arrays;

public class LeetCode1005 {
    public static int largestSumAfterKNegations(int[] nums, int k) {
        for (int i = 1; i <= k; i++) {
            int index = 0;
            for (int a = 1; a < nums.length; a++) {
                if (nums[index] > nums[a]) {
                    index = a;
                }
            }
            nums[index] = -nums[index];
        }
        int count = 0;
        for (int num : nums) {
            count += num;
        }
        return count;
    }

    public static int largestSumAfterKNegations2(int[] nums, int k) {
        int count = 0;
        Arrays.sort(nums);
        int minNum = 101;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < 0 && k > 0) {
                nums[i] = 0 - nums[i];
                k--;
            }
            minNum = Math.min(minNum, nums[i]);
            count += nums[i];
        }
        count = k % 2 == 0 ? count : count - 2 * minNum;
        return count;
    }

    public static void main(String[] args) {
        System.out.println(largestSumAfterKNegations2(new int[]{-4, -2, -3}, 4));
    }
}
