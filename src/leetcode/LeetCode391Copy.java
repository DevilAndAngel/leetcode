package leetcode;

import java.util.*;
import java.awt.*;

public class LeetCode391Copy {
    public boolean isRectangleCover(int[][] rectangles) {
        Map<Point, Integer> maps = new HashMap();
        long area = 0;
        for (int[] nums : rectangles) {
            maps.put(new Point(nums[0], nums[1]), maps.getOrDefault(new Point(nums[0], nums[1]), 0) + 1);
            maps.put(new Point(nums[0], nums[3]), maps.getOrDefault(new Point(nums[0], nums[3]), 0) + 1);
            maps.put(new Point(nums[2], nums[1]), maps.getOrDefault(new Point(nums[2], nums[1]), 0) + 1);
            maps.put(new Point(nums[2], nums[3]), maps.getOrDefault(new Point(nums[2], nums[3]), 0) + 1);
            area += (nums[2] - nums[0]) * (nums[3] - nums[1]);
        }

        int count = 0;
        for (Point nums : maps.keySet()) {
            if (maps.get(nums) == 1) {
                count += 1;
            }
            if (maps.get(nums) != 2 && maps.get(nums) != 4 && count > 4) {
                return false;
            }
        }
        return true;
    }
}
