package leetcode;

import java.util.ArrayList;
import java.util.List;

public class leetCode02 {
    static int n = 2126753390;
    static int version = 1702766719;
    static List<Boolean> list = new ArrayList();

    static {
        for (int i = 1; i <= n; i++) {
            if (i < version) {
                list.add(false);
            } else {
                list.add(true);
            }
        }
    }

    public static void main(String[] args) {
        int startIndex = 1;
        int endIndex = n;
        while (endIndex - startIndex > 1) {
            int index = (startIndex + endIndex) / 2;
            if (isBadVersion(index)) {
                endIndex = index;
            } else {
                startIndex = index;
            }
        }
        System.out.println(endIndex);
    }

    public static boolean isBadVersion(int index) {
        return list.get(version);
    }
}
