package leetcode;

public class LeetCode299 {
    public String getHint(String secret, String guess) {
        char[] sChar = secret.toCharArray();
        char[] gChar = guess.toCharArray();
        int a = 0, b = 0;
        for (int i = 0; i < sChar.length; i++) {
            if (i < gChar.length && sChar[i] == gChar[i]) {
                a++;
                sChar[i] = 'b';
                gChar[i] = 'a';
            }
        }
        for (int i = 0; i < sChar.length; i++) {
            for (int j = 0; j < gChar.length; j++) {
                if (sChar[i] == gChar[j]) {
                    b++;
                    sChar[i] = 'b';
                    gChar[j] = 'a';
                    break;
                }
            }
        }
        return a + "A" + b + "B";
    }
}
