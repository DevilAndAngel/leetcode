package leetcode;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class LeetCode391 {
    public boolean isRectangleCover(int[][] rectangles) {
        Map<Point, Integer> map = new HashMap<>();
        long count = 0;
        int minX = rectangles[0][0], minY = rectangles[0][1], maxA = rectangles[0][2], maxB = rectangles[0][3];
        for (int[] nums : rectangles) {
            count = count + (nums[2] - nums[0]) * (nums[3] - nums[1]);

            minX = Math.min(minX, nums[0]);
            minY = Math.min(minY, nums[1]);
            maxA = Math.max(maxA, nums[2]);
            maxB = Math.max(maxB, nums[3]);

            Point point1 = new Point(nums[0], nums[1]);
            Point point2 = new Point(nums[0], nums[3]);
            Point point3 = new Point(nums[2], nums[1]);
            Point point4 = new Point(nums[2], nums[3]);

            map.put(point1, map.getOrDefault(point1, 0) + 1);
            map.put(point2, map.getOrDefault(point2, 0) + 1);
            map.put(point3, map.getOrDefault(point3, 0) + 1);
            map.put(point4, map.getOrDefault(point4, 0) + 1);
        }
        if (count != ((maxA - minX) * (maxB - minY))
                || map.getOrDefault(new Point(minX, minY), 0) != 1
                || map.getOrDefault(new Point(minX, maxB), 0) != 1
                || map.getOrDefault(new Point(maxA, minY), 0) != 1
                || map.getOrDefault(new Point(maxA, maxB), 0) != 1) {
            return false;
        }

        map.remove(new Point(minX, minY));
        map.remove(new Point(minX, maxB));
        map.remove(new Point(maxA, minY));
        map.remove(new Point(maxA, maxB));

        for (Point point : map.keySet()) {
            if (map.get(point) != 2 && map.get(point) != 4) {
                return false;
            }
        }

        return true;
    }
}
