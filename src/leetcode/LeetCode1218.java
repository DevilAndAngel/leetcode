package leetcode;

import java.util.HashMap;
import java.util.Map;

public class LeetCode1218 {
    public int longestSubsequence(int[] arr, int difference) {
        int ans = 0;
        Map<Integer, Integer> map = new HashMap();
        for (int v : arr) {
            map.put(v, map.getOrDefault(v - difference, 0) + 1);
            ans = Math.max(ans, map.get(v));
        }
        return ans;
    }
}
